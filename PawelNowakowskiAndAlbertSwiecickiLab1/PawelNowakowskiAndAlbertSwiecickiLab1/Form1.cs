﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PawelNowakowskiAndAlbertSwiecickiLab1
{
    public partial class Form1 : Form
    {
        public int clicker;
        /// <summary>
        /// konstruktor inicjalizujacy zmienne, w tym ustawiajacy maksymalna
        /// wartosc progressbar1 na 50 oraz krok na 1
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            clicker = 0;
            progressBar1.Maximum = 50;
            progressBar1.Step = 1;
        }
        /// <summary>
        /// metoda obslugujaca zdarzenie klikniecia przycisku
        /// jezeli wpiszemy w okienko loginu i hasla "test" otworzy nam sie nowe okno
        /// jesli wczesniej kliknelismy przycisk progress
        /// w nowym oknie w miejscu na login i haslo bedziemy mieli haslo i login z poprzedniego okna
        /// przy wprowadznie blednych danych okno zmienia kolor na czerwony
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBoxpassword.Text=="test") && (textBoxLogin.Text == "test"))
            {
                if (clicker > progressBar1.Maximum)
                {
                    progressBar1.Value = 0;
                    MessageBox.Show("");
                    Form1 form = new Form1();
                    form.textBoxLogin.Text = textBoxLogin.Text;
                    form.textBoxpassword.Text = textBoxpassword.Text;
                    form.Show();
                }
            }
            else {
                MessageBox.Show("Incorrect login!!!");
                BackColor = Color.Red;
            }

        }
        /// <summary>
        /// metoda inkrementujaca zmienna clicker oraz obslugujaca Tick timera
        /// po czasie ustawionym na timerze wywoluje sie ta metoda powodujac krok na progressbar1
        /// oraz inkrementujaca zmienna clicker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.progressBar1.PerformStep();
            clicker++;

        }
        /// <summary>
        /// Metoda wlaczajaca timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clickMe_Click(object sender, EventArgs e)
        {
            this.timer1.Start();
        }
    }
}
