﻿namespace PawelNowakowskiGraphics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelIncrement = new System.Windows.Forms.Label();
            this.labelLength = new System.Windows.Forms.Label();
            this.labelAngle = new System.Windows.Forms.Label();
            this.labelLines = new System.Windows.Forms.Label();
            this.textBoxIncrement = new System.Windows.Forms.TextBox();
            this.textBoxLength = new System.Windows.Forms.TextBox();
            this.textBoxAngle = new System.Windows.Forms.TextBox();
            this.textBoxLines = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.labelIncrement);
            this.panel1.Controls.Add(this.labelLength);
            this.panel1.Controls.Add(this.labelAngle);
            this.panel1.Controls.Add(this.labelLines);
            this.panel1.Controls.Add(this.textBoxIncrement);
            this.panel1.Controls.Add(this.textBoxLength);
            this.panel1.Controls.Add(this.textBoxAngle);
            this.panel1.Controls.Add(this.textBoxLines);
            this.panel1.Controls.Add(this.buttonStart);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(580, 63);
            this.panel1.TabIndex = 0;
            // 
            // labelIncrement
            // 
            this.labelIncrement.AutoSize = true;
            this.labelIncrement.Location = new System.Drawing.Point(115, 29);
            this.labelIncrement.Name = "labelIncrement";
            this.labelIncrement.Size = new System.Drawing.Size(54, 13);
            this.labelIncrement.TabIndex = 8;
            this.labelIncrement.Text = "Increment";
            // 
            // labelLength
            // 
            this.labelLength.AutoSize = true;
            this.labelLength.Location = new System.Drawing.Point(115, 9);
            this.labelLength.Name = "labelLength";
            this.labelLength.Size = new System.Drawing.Size(40, 13);
            this.labelLength.TabIndex = 7;
            this.labelLength.Text = "Length";
            // 
            // labelAngle
            // 
            this.labelAngle.AutoSize = true;
            this.labelAngle.Location = new System.Drawing.Point(12, 29);
            this.labelAngle.Name = "labelAngle";
            this.labelAngle.Size = new System.Drawing.Size(34, 13);
            this.labelAngle.TabIndex = 6;
            this.labelAngle.Text = "Angle";
            // 
            // labelLines
            // 
            this.labelLines.AutoSize = true;
            this.labelLines.Location = new System.Drawing.Point(12, 9);
            this.labelLines.Name = "labelLines";
            this.labelLines.Size = new System.Drawing.Size(32, 13);
            this.labelLines.TabIndex = 5;
            this.labelLines.Text = "Lines";
            // 
            // textBoxIncrement
            // 
            this.textBoxIncrement.Location = new System.Drawing.Point(176, 29);
            this.textBoxIncrement.Name = "textBoxIncrement";
            this.textBoxIncrement.Size = new System.Drawing.Size(47, 20);
            this.textBoxIncrement.TabIndex = 4;
            this.textBoxIncrement.Text = "1";
            // 
            // textBoxLength
            // 
            this.textBoxLength.Location = new System.Drawing.Point(176, 6);
            this.textBoxLength.Name = "textBoxLength";
            this.textBoxLength.Size = new System.Drawing.Size(47, 20);
            this.textBoxLength.TabIndex = 3;
            this.textBoxLength.Text = "5";
            // 
            // textBoxAngle
            // 
            this.textBoxAngle.Location = new System.Drawing.Point(52, 26);
            this.textBoxAngle.Name = "textBoxAngle";
            this.textBoxAngle.Size = new System.Drawing.Size(47, 20);
            this.textBoxAngle.TabIndex = 2;
            this.textBoxAngle.Text = "75";
            // 
            // textBoxLines
            // 
            this.textBoxLines.Location = new System.Drawing.Point(52, 6);
            this.textBoxLines.Name = "textBoxLines";
            this.textBoxLines.Size = new System.Drawing.Size(47, 20);
            this.textBoxLines.TabIndex = 1;
            this.textBoxLines.Text = "100";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(261, 19);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(580, 342);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 405);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox textBoxLines;
        private System.Windows.Forms.Label labelLines;
        private System.Windows.Forms.TextBox textBoxIncrement;
        private System.Windows.Forms.TextBox textBoxLength;
        private System.Windows.Forms.TextBox textBoxAngle;
        private System.Windows.Forms.Label labelIncrement;
        private System.Windows.Forms.Label labelLength;
        private System.Windows.Forms.Label labelAngle;
    }
}

