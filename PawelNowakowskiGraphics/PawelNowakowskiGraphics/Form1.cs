﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PawelNowakowskiGraphics
{
    public partial class Form1 : Form
    {

        Pen myPen = new Pen(Color.Blue);
        Graphics g = null;

        static int startX, startY;
        static int endX, endY;

        static int myAngle = 0;
        static int myLength = 0;
        static int myIncrement = 0;
        /// <summary>
        /// konstruktor klasy inicjalizuje wpolrzedna startowa x i y
        /// na podstawie wysokosci i szerokosci panelu
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            startX = panel2.Width / 2;
            startY = panel2.Height / 2; 
        }
        /// <summary>
        /// ustawienie grubosci naszego markera na 1
        /// przypisanie do myLength wartosci z textboxa (rzutowanie na int)
        /// przypisanie do naszej klasy graphics panelu2 gdzie bedzie nasza mozajka
        /// nastepenie w petli rusowanie kolejnych lini za pomoca metody DrawLines oraz klasy Graphisc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            myPen.Width = 1;
            myLength = Int32.Parse(textBoxLength.Text);
            g = panel2.CreateGraphics();
            for (int i = 0; i < Int32.Parse(textBoxLines.Text); i++)
                DrawLines();
        }

        /// <summary>
        /// metoda odpowiedzialna za liczenie kolejnych punktow do rysowania oraz za samo rysowanie
        /// wyliczonych juz punktow
        /// za pomoca klasy random kolejne linie beda mialy rozne kolory
        /// odczytanie parametrow z textboxow oraz 
        /// rzutowanie na i przypisanie do odpowiednich zmiennych
        /// wyliczenie punktow koncowych na podstawie poczatkowych
        /// tworzymy tablice dwoch punktow ktora posluzy nam za linie 
        /// przypisanie punktu poczatkowego do koncowego oraz namalowanie naszych prostych za pomoca
        /// klasy graphics a dokladniej metody DrawLines
        /// </summary>
        private void DrawLines()
        {
            Random randomGem = new Random();
            myPen.Color = Color.FromArgb(randomGem.Next(100), randomGem.Next(100), randomGem.Next(100));
            myAngle = myAngle + Int32.Parse(textBoxAngle.Text);
            myLength = myLength + Int32.Parse(textBoxIncrement.Text);

            endX = (int)(startX + Math.Cos(myAngle * .017) * myLength);
            endY = (int)(startX + Math.Sin(myAngle * .017) * myLength);

            Point[] points = 
            {
                new Point(startX, startY),
                new Point(endX, endY)
            };

            startX = endX;
            startY = endY;

            g.DrawLines(myPen, points);
        }
        /// <summary>
        /// metoda opslugujaca zdarzenie klikniecia przycisku
        /// odczytanie danych z textboxow, rzutowanie na int i przypisanie do zmiennych
        /// obliczenie punktu startowego na ppodstawie wielkosci panelu
        /// na koniec odswierzamy panel na ktoreym bedzie nasz rysunek
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            myLength = Int32.Parse(textBoxLength.Text);
            myAngle = Int32.Parse(textBoxAngle.Text);
            myIncrement = Int32.Parse(textBoxIncrement.Text);

            startX = panel2.Width / 2;
            startY = panel2.Height / 2;

            panel2.Refresh();
        }


    }
}
